package com.example.turista.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

// Declares the DAO as a private property in the constructor. Pass in the DAO
// instead of the whole database, because you only need access to the DAO

class TuristaRepository(private val turistaDao: TuristaDao, private val languageDao: LanguageDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allTourists: LiveData<List<Turista>> = turistaDao.getAlphabetizedTurists()
    val allLanguages: LiveData<List<Language>> = languageDao.getLanguages()

    suspend fun insert(turista: Turista) {
        turistaDao.insert(turista)
    }

    suspend fun updateLanguage( code: String, status:String)
    {
        languageDao.updateLanguage(code,status)
    }
}