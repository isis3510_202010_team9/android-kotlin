package com.example.turista.model
import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

// Annotates class to be a Room Database with a table (entity) of the Word class
@Database(entities = arrayOf(Turista::class,Language::class), version = 2, exportSchema = false)
public abstract class TuristaRoomDatabase : RoomDatabase() {

    abstract fun turistaDao(): TuristaDao
    abstract fun languageDao(): LanguageDao

    private class TuristaDatabaseCallback(
        private val scope: CoroutineScope

    ) : RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
        }

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    populateDatabase(database.languageDao())
                }
            }
        }

        suspend fun populateDatabase(languageDao: LanguageDao) {
            // Delete all content here.


            languageDao.insert(Language("af","NO_DOWNLOADED","Afrikaans"))
            languageDao.insert(Language("ar","NO_DOWNLOADED","Arabic"))
            languageDao.insert(Language("be","NO_DOWNLOADED","Belarusian"))
            languageDao.insert(Language("bg","NO_DOWNLOADED","Bulgarian"))
            languageDao.insert(Language("bn","NO_DOWNLOADED","Bengali"))
            languageDao.insert(Language("ca","NO_DOWNLOADED","Catalan"))
            languageDao.insert(Language("cs","NO_DOWNLOADED","Czech"))
            languageDao.insert(Language("cy","NO_DOWNLOADED","Welsh"))
            languageDao.insert(Language("da","NO_DOWNLOADED","Danish"))
            languageDao.insert(Language("de","NO_DOWNLOADED","German"))
            languageDao.insert(Language("el","NO_DOWNLOADED","Greek"))
            languageDao.insert(Language("en","DOWNLOADED","English"))
            languageDao.insert(Language("eo","NO_DOWNLOADED","Esperanto"))
            languageDao.insert(Language("es","NO_DOWNLOADED","Spanish"))
            languageDao.insert(Language("et","NO_DOWNLOADED","Estonian"))
            languageDao.insert(Language("fa","NO_DOWNLOADED","Persian"))
            languageDao.insert(Language("fi","NO_DOWNLOADED","Finnish"))
            languageDao.insert(Language("fr","NO_DOWNLOADED","French"))
            languageDao.insert(Language("ga","NO_DOWNLOADED","Irish"))
            languageDao.insert(Language("gl","NO_DOWNLOADED","Galician"))
            languageDao.insert(Language("gu","NO_DOWNLOADED","Gujarati"))
            languageDao.insert(Language("he","NO_DOWNLOADED","Hebrew"))
            languageDao.insert(Language("hi","NO_DOWNLOADED","Hindi"))
            languageDao.insert(Language("hr","NO_DOWNLOADED","Croatian"))
            languageDao.insert(Language("ht","NO_DOWNLOADED","Haitian"))
            languageDao.insert(Language("hu","NO_DOWNLOADED","Hungarian"))
            languageDao.insert(Language("id","NO_DOWNLOADED","Indonesian"))
            languageDao.insert(Language("is","NO_DOWNLOADED","Icelandic"))
            languageDao.insert(Language("it","NO_DOWNLOADED","Italian"))
            languageDao.insert(Language("ja","NO_DOWNLOADED","Japanese"))
            languageDao.insert(Language("ka","NO_DOWNLOADED","Georgian"))
            languageDao.insert(Language("kn","NO_DOWNLOADED","Kannada"))
            languageDao.insert(Language("ko","NO_DOWNLOADED","Korean"))
            languageDao.insert(Language("lt","NO_DOWNLOADED","Lithuanian"))
            languageDao.insert(Language("lv","NO_DOWNLOADED","Latvian"))
            languageDao.insert(Language("mk","NO_DOWNLOADED","Macedonian"))
            languageDao.insert(Language("mr","NO_DOWNLOADED","Marathi"))
            languageDao.insert(Language("ms","NO_DOWNLOADED","Malay"))
            languageDao.insert(Language("mt","NO_DOWNLOADED","Maltese"))
            languageDao.insert(Language("nl","NO_DOWNLOADED","Dutch"))
            languageDao.insert(Language("no","NO_DOWNLOADED","Norwegian"))
            languageDao.insert(Language("pl","NO_DOWNLOADED","Polish"))
            languageDao.insert(Language("pt","NO_DOWNLOADED","Portuguese"))
            languageDao.insert(Language("ro","NO_DOWNLOADED","Romanian"))
            languageDao.insert(Language("ru","NO_DOWNLOADED","Russian"))
            languageDao.insert(Language("sk","NO_DOWNLOADED","Slovak"))
            languageDao.insert(Language("sl","NO_DOWNLOADED","Slovenian"))
            languageDao.insert(Language("sq","NO_DOWNLOADED","Albanian"))
            languageDao.insert(Language("sv","NO_DOWNLOADED","Swedish"))
            languageDao.insert(Language("sw","NO_DOWNLOADED","Swahili"))
            languageDao.insert(Language("ta","NO_DOWNLOADED","Tamil"))
            languageDao.insert(Language("te","NO_DOWNLOADED","Telugu"))
            languageDao.insert(Language("th","NO_DOWNLOADED","Thai"))
            languageDao.insert(Language("tl","NO_DOWNLOADED","Tagalog"))
            languageDao.insert(Language("tr","NO_DOWNLOADED","Turkish"))
            languageDao.insert(Language("uk","NO_DOWNLOADED","Ukrainian"))
            languageDao.insert(Language("ur","NO_DOWNLOADED","Urdu"))
            languageDao.insert(Language("vi","NO_DOWNLOADED","Vietnamese"))
            languageDao.insert(Language("zh","NO_DOWNLOADED","Chinese"))


        }
    }

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: TuristaRoomDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): TuristaRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    TuristaRoomDatabase::class.java,
                    "touristapp_db"
                )
                .addCallback(TuristaDatabaseCallback(scope))
                .build()
                INSTANCE = instance
                return instance
            }
        }
    }


}