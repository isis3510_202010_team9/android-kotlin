package com.example.turista.model

import androidx.room.*

@Entity(tableName = "turist_table")
data class Turista(
    @PrimaryKey var id: String,
    var firstName: String?,
    var lastName: String?
)