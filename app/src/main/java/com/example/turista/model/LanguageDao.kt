package com.example.turista.model
import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface LanguageDao {

    @Query("SELECT * from language_table")
    fun getLanguages(): LiveData<List<Language>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(language: Language)

    @Query("DELETE FROM language_table")
    suspend fun deleteAll()

    @Query("UPDATE language_table SET status = :status WHERE code = :code")
    fun updateLanguage(code: String, status: String): Int
}