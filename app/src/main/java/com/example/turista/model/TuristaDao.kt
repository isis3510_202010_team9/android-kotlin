package com.example.turista.model
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface TuristaDao {

    @Query("SELECT * from turist_table ORDER BY firstName ASC")
    fun getAlphabetizedTurists(): LiveData<List<Turista>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(turista: Turista)

    @Query("DELETE FROM turist_table")
    suspend fun deleteAll()
}