package com.example.turista.model

import androidx.room.*

@Entity(tableName = "language_table")
data class Language(
    @PrimaryKey var code: String,
    var status: String,
    var language: String?
)