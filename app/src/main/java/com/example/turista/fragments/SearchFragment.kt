package com.example.turista.fragments


import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.turista.R
import com.example.turista.activities.InternetCheck
import com.example.turista.activities.TipActivity
import com.google.android.gms.maps.model.LatLng
import java.io.IOException
import kotlin.random.Random


class SearchFragment : Fragment() {
    companion object {
        fun newInstance(): SearchFragment =
            SearchFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater!!.inflate(R.layout.fragment_search, container, false)
        val sitp_img: ImageView = view.findViewById(R.id.sitp_img)
        val tm_img: ImageView = view.findViewById(R.id.tm_img)
        val tax_img: ImageView = view.findViewById(R.id.tax_img)
        val sitpprov_img: ImageView = view.findViewById(R.id.sitpprov_img)
        val walk_img: ImageView = view.findViewById(R.id.walk_img)
        val gen_img: ImageView = view.findViewById(R.id.gen_img)
        val tipgen: TextView = view.findViewById(R.id.tipgen)

        tipgen.text = random_tip()

        val search_bar: SearchView = view.findViewById(R.id.seach_place)
        search_bar.setOnQueryTextListener(object :  SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchLocation(query)
                return false
            }

        })

        /*val autocompleteFragment = view.findViewById(R.id.seach_place)
                as PlaceAutocompleteFragment
        autocompleteFragment.setOnPlaceSelectedListener(this)*/
        

        sitp_img.setOnClickListener {view ->
            val intent = Intent(view.context,TipActivity::class.java);
            var transport = "sitp"
            intent.putExtra("transport",transport)
            startActivity(intent)
        }
        tm_img.setOnClickListener {view ->
            val intent = Intent(view.context,TipActivity::class.java);
            var transport = "tm"
            intent.putExtra("transport",transport)
            startActivity(intent)
        }
        tax_img.setOnClickListener {view ->
            val intent = Intent(view.context,TipActivity::class.java);
            var transport = "tax"
            intent.putExtra("transport",transport)
            startActivity(intent)
        }
        sitpprov_img.setOnClickListener {view ->
            val intent = Intent(view.context,TipActivity::class.java);
            var transport = "sitpprov"
            intent.putExtra("transport",transport)
            startActivity(intent)
        }
        walk_img.setOnClickListener {view ->
            val intent = Intent(view.context,TipActivity::class.java);
            var transport = "walk"
            intent.putExtra("transport",transport)
            startActivity(intent)
        }
        gen_img.setOnClickListener {view ->
            val intent = Intent(view.context,TipActivity::class.java);
            var transport = "general"
            intent.putExtra("transport",transport)
            startActivity(intent)
        }

        return view
    }

    /*override fun onPlaceSelected(p0: Place?) {

        Toast.makeText(applicationContext,""+p0!!.name+p0!!.latLng,Toast.LENGTH_LONG).show();
    }

    override fun onError(status: Status) {
        Toast.makeText(applicationContext,""+status.toString(),Toast.LENGTH_LONG).show();
    }*/

    fun random_tip(): String {
        val tips = arrayOf("Plan ahead your\ntrip, but don't \ndiscard new ideas",
        "Keep change and bills,\nsome places do not\nreceive credit card",
            "Be alert at all time,\ndouble check before\nyou cross a street",
        "Have a spare jacket\n or umbrella, weather\n is always a surprise")
        val num = Random.nextInt(0,3)
        return tips[num]

    }

    fun searchLocation(location: String) {
        //val locationSearch: SearchView = view.findViewById(R.id.seach_place)
        //lateinit var location: String
        //location = locationSearch. as String


        if (location == null || location == "") {
            Toast.makeText(this.context,"Provide a location",Toast.LENGTH_SHORT).show()
        }
        else{
            try {
                var netstatus = false
                Thread {
                    //Do some Network Request
                    var ie = InternetCheck()
                    netstatus = ie.net()
                    activity?.runOnUiThread {
                        if(!netstatus){
                            showAlert("You can't search new places without net Connection")
                        }else{
                            do_search(location)
                        }
                    }
                }.start()


            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    private fun do_search(location: String){
        val geoCoder = Geocoder(this.context)
        var addressList: List<Address>? = null
        addressList = geoCoder.getFromLocationName(location, 5,4.36,-74.27,5.06,-73.9)
        var placeurl= "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input="
        placeurl+=tourl(location)
        placeurl+="&inputtype=textquery&fields=photos,formatted_address,name,opening_hours,rating&locationbias=rectangle:4.36,-74.27,5.06,-73.9&key=AIzaSyAzRgW8Mh44Fw4jKhBhBsnkuta16tvJGL4"

        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, placeurl, null,
            Response.Listener { response ->
                //val routes = response.getJSONArray("routes")
                //val legs = routes.getJSONObject(0).getJSONArray("legs")
                //val steps = legs.getJSONObject(0).getJSONArray("steps")
                print(response)
            },
            Response.ErrorListener { error ->
                showAlert("Unable to find the place")
            }
        )


        if (addressList!!.isEmpty()){
            showAlert("No places found under that name, try again")
        }else {
            val address = addressList!![0]
            //TODO
            //val places :Places = Places(this.context)
            val url = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyAzRgW8Mh44Fw4jKhBhBsnkuta16tvJGL4&place_id="


            val latLng = LatLng(address.latitude, address.longitude)
            Toast.makeText(
                this.context,
                address.latitude.toString() + " " + address.longitude,
                Toast.LENGTH_LONG
            ).show()

            val mFragmentManager: FragmentManager = this.activity!!.supportFragmentManager

            val fragment: HomeFragment =
                mFragmentManager.findFragmentByTag(HomeFragment::class.java
                    .getSimpleName()) as HomeFragment
            fragment.addmarker(location,latLng)
        }
    }

    private fun tourl(location: String): String {
        location.replace(" ","%20")
        return location
    }

    private fun showAlert(message: String) {
        val dialog = this.context?.let { AlertDialog.Builder(it) }
        if (dialog != null) {
            dialog.setTitle("Error")
            dialog.setMessage(message)
            dialog.setPositiveButton(" OK ",
                { dialog, id -> dialog.dismiss() })
            dialog.show()
        }


    }


}