package com.example.turista.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.media.Image
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.example.turista.R
import com.example.turista.activities.FromToLanguage
import com.example.turista.activities.InternetCheck
import com.example.turista.activities.SignUpActivity
import com.example.turista.databinding.FragmentTranslateBinding
import com.example.turista.model.Language
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager
import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslateLanguage
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslateRemoteModel
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslator
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslatorOptions
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import kotlinx.android.synthetic.main.fragment_translate.*

const val FROM_REQUEST = 1
const val TO_REQUEST = 2
const val IMAGE_REQUEST = 3

class TranslateFragment : Fragment(), View.OnClickListener {
    private var _binding: FragmentTranslateBinding? = null
    private val binding get() = _binding!!
    lateinit var imageView: ImageView
    private var mContext: Context? = null
    private var firebaseAnalytics: FirebaseAnalytics? = null
    private var fetchedData = false
    private  var sourceStatus : String = "NO_STATUS"
    private  var targetStatus : String = "NO_STATUS"

    private var sourceLanguage = -1
    private var targetLanguage = -1
    private var areModelsDownloaded = false
    private var currentTranslator: FirebaseTranslator? = null
    private var models = mutableSetOf<FirebaseTranslateRemoteModel>()
    private var stringPassed = ""
    private var modelManager: FirebaseModelManager = FirebaseModelManager.getInstance()
    private lateinit var availableModels : ArrayList<String>

    companion object {
        fun newInstance(): TranslateFragment =
            TranslateFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        modelManager = FirebaseModelManager.getInstance()

        fetchDownloadedModels()

        availableModels = ArrayList()
        _binding = FragmentTranslateBinding.inflate(inflater, container, false)
        val view = binding.root
        imageView = view.findViewById(R.id.image)
        binding.sourceTextField.movementMethod = ScrollingMovementMethod()
        binding.targetTextField.movementMethod = ScrollingMovementMethod()

        binding.imageButton.setOnClickListener{view ->
            selectImage(view)
        }

        binding.translateButton.setOnClickListener{ view ->
            translate()
        }
        binding.fromButton.setOnClickListener{ view ->
            if(fetchedData) {
                Log.d("Modelos", availableModels.toString())
                val intentFrom = Intent(mContext, FromToLanguage::class.java)
                intentFrom.putExtra("title", "Translate from")
                intentFrom.putExtra("Fetched", availableModels)
                startActivityForResult(intentFrom,FROM_REQUEST)
                Log.d("Modelos", "Llegooooo")
            }
        }
        binding.toButton.setOnClickListener{  view ->
            if(fetchedData) {
                Log.d("Modelos", availableModels.toString())
                val intentFrom = Intent(mContext, FromToLanguage::class.java)
                intentFrom.putExtra("title", "Translate to")
                intentFrom.putExtra("Fetched", availableModels)
                startActivityForResult(intentFrom, TO_REQUEST)
            }
        }
        binding.changeOrderButton.setOnClickListener{  view ->

            if(sourceLanguage==-1||targetLanguage==-1){
                showAlert("You must specify two languages for translation")
            }
            else{

                var temporal = sourceLanguage
                sourceLanguage = targetLanguage
                targetLanguage = temporal

                var temporal2 = binding.fromButton.text
                binding.fromButton.text = binding.toButton.text
                binding.toButton.text = temporal2
            }

        }



        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(mContext!!)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onClick(v: View?) {
        when (view!!.id) {
       /*     R.id.view -> {
                //realiza operación al dar clic al imageView.
                val intent = Intent(this, LevelActivity::class.java)
                startActivity(intent)
            }
            else -> {
            }*/
        }

    }

    // Fetch a translator based on the language selected by the user.

    private fun translate(){
        hideKeyboard()
        if (sourceTextField.text.isBlank()){
            showAlert("Please enter a text to translate")
            return
        }
        if (sourceLanguage == -1 || targetLanguage == -1){
            showAlert("Please choose languages")
            return
        }
       /* progressBar.visibility = View.VISIBLE*/
        currentTranslator = prepareATranslatorWith(sourceLanguage, targetLanguage)

    }

    private fun setUpPickers(){
/*        val data = arrayOf("None","English", "Swedish","German", "French")
        var fromPicker = fromPicker
        fromPicker.minValue = 0
        fromPicker.maxValue = data.size - 1
        fromPicker.displayedValues = data
        fromPicker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        fromPicker.setOnValueChangedListener{picker, oldvalue, newvalue ->

            sourceLanguage = getFirebaseLanguageFrom(data[newvalue])
        }

        var toPicker = toPicker
        toPicker.minValue = 0
        toPicker.maxValue = data.size - 1
        toPicker.displayedValues = data
        toPicker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        toPicker.setOnValueChangedListener{picker, oldvalue, newvalue ->

            targetLanguage = getFirebaseLanguageFrom(data[newvalue])
        }*/
    }

    fun downloadLanguage(code: String) {
        val model = FirebaseTranslateRemoteModel.Builder(FirebaseTranslateLanguage.languageForLanguageCode(code)!!).build()

        val conditions = FirebaseModelDownloadConditions.Builder()
            .requireWifi()
            .build()
        modelManager.download(model, conditions)
            .addOnCompleteListener { fetchDownloadedModels() }
    }

// ...

    private fun fetchDownloadedModels() {
        modelManager.getDownloadedModels(FirebaseTranslateRemoteModel::class.java)
            .addOnSuccessListener { remoteModels ->
                remoteModels.map { availableModels.add(it.languageCode) }
                fetchedData = true

            }

    }
    // Fetch a translator based on the language selected by the user.
    private fun prepareATranslatorWith(sourceLang: Int, destinationLang:Int ): FirebaseTranslator {

        // Create an English-German translator:
        val options = FirebaseTranslatorOptions.Builder()
            .setSourceLanguage(sourceLang)
            .setTargetLanguage(destinationLang)
            .build()
        val translator = FirebaseNaturalLanguage.getInstance().getTranslator(options)
        // Check for model
        Thread {
            //Do some Network Request
            var ie = InternetCheck()
            var netstatus = ie.net()
            activity?.runOnUiThread {
                if((sourceStatus == "NO_DOWNLOADED" || targetStatus == "NO_DOWNLOADED")&&!netstatus){
                    showAlert("You don´t have internet connection to download the required model(s)")
                }
            }
        }.start()
        translator.downloadModelIfNeeded()
            .addOnSuccessListener {
                areModelsDownloaded = true
                activity?.runOnUiThread() {

                    translateTextNow()
                    translator.close()
                }

            }
            .addOnCanceledListener {
                showAlert("The model(s) download was canceled, try again")
            }
            .addOnFailureListener{
                showAlert("The model(s) download was canceled, try again")

            }

        return  translator
    }


    private fun showAlert(message: String) {
        val dialog = AlertDialog.Builder(mContext!!)
        dialog.setTitle("Error")
        dialog.setMessage(message)
        dialog.setPositiveButton(" OK ",
            { dialog, id -> dialog.dismiss() })
        dialog.show()

    }

    private fun translateTextNow(){
        currentTranslator!!.translate(sourceTextField.text.toString())
            .addOnSuccessListener { translatedText ->
                activity?.runOnUiThread{
                    targetTextField.text = translatedText
                    Log.d("Translate", "Traduje el texto")
                    val params = Bundle()
                    params.putString("source_language", from_button.text.toString())
                    params.putString("target_language", to_button.text.toString())
                    params.putString("input_text", sourceTextField.text.toString())
                    params.putString("translation", translatedText)
                    firebaseAnalytics?.logEvent("translation", params)
                    fetchedData = false
                    fetchDownloadedModels()
                }
            }
            .addOnFailureListener {
                activity?.runOnUiThread{
                    showAlert("Failed to translate")
                }
            }

        currentTranslator = null;

    }

    private fun hideKeyboard() {
        // Check if no view has focus:
        val view = activity?.currentFocus
        if (view != null) {
            val inputManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("Fragment", "LLEGOOOOO")
        Log.d("TRANSLATE", "$requestCode----$resultCode")
        // Check which request we're responding to
        if (requestCode == FROM_REQUEST) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                // The user picked a contact.
                if (data != null) {
                    from_button.text = data.extras?.getString("language_choosen_language")
                    var code: String = data.extras?.getString("language_choosen_code")!!
                    sourceStatus = data.extras?.getString("language_status")!!
                    val model = FirebaseTranslateRemoteModel.Builder(
                        FirebaseTranslateLanguage.languageForLanguageCode(code)!!
                    ).build()
                    sourceLanguage = model.language


                    //InternetCheck{ netstatus-> Log.d("Connection", "Is connection enabled? "+netstatus)}

                    if ((data.extras?.getString("language_status")!! == "NO_DOWNLOADED") && !data.extras?.getBoolean(
                            "internet"
                        )!!
                    ) {
                        showAlert(
                            "You need internet connection to download the " + data.extras?.getString(
                                "language_choosen_language"
                            ) + " model"
                        )
                    }
                }
                // The Intent's data Uri identifies which contact was selected.
                // Do something with the contact here (bigger example below)
            }
        }

        if (requestCode == TO_REQUEST) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                // The user picked a contact.
                if (data != null) {

                    to_button.text = data.extras?.getString("language_choosen_language")
                    var code: String = data.extras?.getString("language_choosen_code")!!
                    targetStatus = data.extras?.getString("language_status")!!
                    val model = FirebaseTranslateRemoteModel.Builder(
                        FirebaseTranslateLanguage.languageForLanguageCode(code)!!
                    ).build()
                    targetLanguage = model.language

                    if ((data.extras?.getString("language_status")!! == "NO_DOWNLOADED") && !data.extras?.getBoolean(
                            "internet"
                        )!!
                    ) {
                        showAlert(
                            "You need internet connection to download the " + data.extras?.getString(
                                "language_choosen_language"
                            ) + " model"
                        )
                    }
                }
                // The Intent's data Uri identifies which contact was selected.
                // Do something with the contact here (bigger example below)
            }
        }

        if (requestCode == IMAGE_REQUEST) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                Log.d("IMAGE", "OK")
                imageView.setImageURI(data!!.data)
                startRecognizing()
            }
        }
    }

    fun selectImage(v: View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_REQUEST)
    }

    fun startRecognizing() {
        if (imageView.drawable != null) {
            sourceTextField.setText("")

            val bitmap = (imageView.drawable as BitmapDrawable).bitmap
            val image = FirebaseVisionImage.fromBitmap(bitmap)
            val detector = FirebaseVision.getInstance().onDeviceTextRecognizer
            detector.processImage(image)
                .addOnSuccessListener { firebaseVisionText ->

                    processResultText(firebaseVisionText)
                }
                .addOnFailureListener {

                    sourceTextField.setText("Your device don't have internet connection to download the ocr model")
                }
        } else {
            Toast.makeText(context, "Select an Image First", Toast.LENGTH_LONG).show()
        }

    }

    private fun processResultText(resultText: FirebaseVisionText) {
        if (resultText.textBlocks.size == 0) {
            sourceTextField.setText("No Text Found")
            return
        }
        for (block in resultText.textBlocks) {
            val blockText = block.text
            sourceTextField.append(blockText + "\n")
        }
    }

}