package com.example.turista.fragments

import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.turista.R
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.PolyUtil
import kotlinx.android.synthetic.main.recyclerview_item.*
import org.json.JSONObject
import java.lang.Exception

class HomeFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var map: GoogleMap
    private lateinit var mpv: MapView
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var lastLocation: Location? = null
    private lateinit var ruta: JSONObject
    private var placelist = mutableMapOf<String,LatLng>()
    private lateinit var thekms: TextView
    private lateinit var themins: TextView

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private val singletonHome = HomeFragment()
        fun getInstance(): HomeFragment {
            return singletonHome
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater!!.inflate(R.layout.fragment_home, container, false)
        mpv= view.findViewById(R.id.elmapa)

        thekms= view.findViewById(R.id.textkms)

        themins= view.findViewById(R.id.textmins)

        mpv.onCreate(savedInstanceState);

        mpv.getMapAsync(this)

        fusedLocationClient = this.activity?.let {
            LocationServices.getFusedLocationProviderClient(
                it
            )
        }!!

        return view
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.getUiSettings().setZoomControlsEnabled(true)
        map.setOnMarkerClickListener(this)
        setUpMap()

    }

    fun update_actual_location(){
        this.activity?.let {
            fusedLocationClient.lastLocation.addOnSuccessListener(it) { location ->
                // Got last known location. In some rare situations this can be null.
                // 3
                if (location != null) {
                    lastLocation = location
                    val currentLatLng = LatLng(location.latitude, location.longitude)
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                }
                else{
                    showAlert("The location is not available. Your place will be saved but we can't show you the route")
                }
            }
        }
    }

    fun paint_route(){
        val path: MutableList<List<LatLng>> = ArrayList()
        var urlDirections = "https://maps.googleapis.com/maps/api/directions/json?origin="
        urlDirections += ""+(lastLocation?.latitude) +","+(lastLocation?.longitude)
        urlDirections += "&destination="+(lastLocation?.latitude) +","+(lastLocation?.longitude)
        urlDirections += "&waypoints=optimize:true|"
        val iter = placelist.iterator()
        if(iter.hasNext()) {
            var iterd = iter.next()
            while (true) {
                var itered = iterd.value
                urlDirections += "via:" + (itered.latitude) + "%2C" + (itered.longitude)
                if (iter.hasNext()) {
                    urlDirections += "%7C"
                    iterd = iter.next()
                }else{
                    break
                }

            }
        }
        urlDirections +="&key=AIzaSyAzRgW8Mh44Fw4jKhBhBsnkuta16tvJGL4&place_id"

        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, urlDirections, null,
            Response.Listener { response ->
                val routes = response.getJSONArray("routes")
                val legs = routes.getJSONObject(0).getJSONArray("legs")
                val steps = legs.getJSONObject(0).getJSONArray("steps")
                this.map!!.clear()
                val tiempo = legs.getJSONObject(0).getJSONObject("duration").getString("text")
                val kms = legs.getJSONObject(0).getJSONObject("distance").getString("text")
                update_togo(tiempo,kms)
                for (i in 0 until steps.length()) {
                    val points = steps.getJSONObject(i).getJSONObject("polyline").getString("points")
                    path.add(PolyUtil.decode(points))
                }
                for (i in 0 until path.size) {
                    this.map!!.addPolyline(PolylineOptions().addAll(path[i]).color(Color.rgb(153,149,234)))
                }

                updatemarkers()
                ruta = response
            },
            Response.ErrorListener { error ->
                showAlert("Unable to build the route")
            }
        )
        val requestQueue = Volley.newRequestQueue(this.context)
        requestQueue.add(jsonObjectRequest)

    }

    fun update_togo(tiempo:String, kms:String){
        thekms.text = kms
        themins.text = tiempo
    }

    fun addmarker(place: String, site: LatLng){
        update_actual_location()
        placelist[place] = site
        if (lastLocation == null) {return}
        paint_route()
    }

    fun updatemarkers(){
        val iter = placelist.iterator()
        if(iter.hasNext()) {
            var iterd = iter.next()
            while (true) {
                map.addMarker(MarkerOptions().position(iterd.value).title(iterd.key))
                if (iter.hasNext()) {
                    iterd = iter.next()
                }else{
                    break
                }

            }
        }
    }

    public override fun onStart() {
        super.onStart()
        mpv.onStart()
    }

    public override fun onResume() {
        super.onResume()
        mpv.onResume()
        try{
            paint_route()
        }catch(e: Exception){

        }

    }

    public override fun onPause() {
        super.onPause()
        mpv.onPause()
    }

    public override fun onStop() {
        super.onStop()
        mpv.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mpv.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mpv.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mpv.onSaveInstanceState(outState)
    }

    override fun onMarkerClick(p0: Marker?) = false

    private fun setUpMap() {
        if (this.context?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
            } != PackageManager.PERMISSION_GRANTED) {
            this.activity?.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            }
            return
        }
        // 1
        map.isMyLocationEnabled = true

        // 2
        this.activity?.let {
            fusedLocationClient.lastLocation.addOnSuccessListener(it) { location ->
                // Got last known location. In some rare situations this can be null.
                // 3
                if (location != null) {
                    lastLocation = location
                    val currentLatLng = LatLng(location.latitude, location.longitude)
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                }
            }
        }
    }

    private fun showAlert(message: String) {
        val dialog = this.context?.let { AlertDialog.Builder(it) }
        if (dialog != null) {
            dialog.setTitle("Error")
            dialog.setMessage(message)
            dialog.setPositiveButton(" OK ",
                { dialog, id -> dialog.dismiss() })
            dialog.show()
        }


    }

}