package com.example.turista.viewmodel
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.turista.model.Language
import com.example.turista.model.Turista
import com.example.turista.model.TuristaRepository
import com.example.turista.model.TuristaRoomDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LanguageViewModel(application: Application) : AndroidViewModel(application) {


    private val repository: TuristaRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allLanguages: LiveData<List<Language>>

    init {
        val db = TuristaRoomDatabase.getDatabase(
            application,
            viewModelScope
        )
        val turistasDao = db.turistaDao()
        val languageDao = db.languageDao()
        repository = TuristaRepository(turistasDao, languageDao)
        allLanguages = repository.allLanguages
    }

    /**
     * Launching a new coroutine to update the data in a non-blocking way
     */
    fun update(code: String, status:String) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateLanguage(code, status)
    }


}