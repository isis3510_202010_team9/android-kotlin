package com.example.turista.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.turista.R
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_auth2.*
import kotlinx.coroutines.*


class AuthActivity : AppCompatActivity() {
    private val TAG = AuthActivity::class.qualifiedName

    private lateinit var auth: FirebaseAuth
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        var policy = StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_auth2)
        auth = FirebaseAuth.getInstance()
        Log.d(TAG, auth?.currentUser?.email.toString())

        btn_sign_up.setOnClickListener {
            startActivityForResult(Intent(this, SignUpActivity::class.java),1)
        }
        btn_login.setOnClickListener{

            doLogin()
        }
       /* initSignInButton();
        initAuthViewModel();
        initGoogleSignInClient();*/
    }

    private fun doLogin() {


        var ie = InternetCheck()
        var netstatus = ie.net()
            //InternetCheck{ netstatus-> Log.d("Connection", "Is connection enabled? "+netstatus)}

        if(!netstatus){
                // If sign in fails, display a message to the user.
            Log.w(TAG, "signInWithEmail:failure")
            Toast.makeText(baseContext, "No internet connection.",
                Toast.LENGTH_SHORT).show()

            return
        }

        if(tv_user.text.toString().isEmpty()){
            tv_user.error = "Please enter email"
            tv_user.requestFocus()
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(tv_user.text.toString()).matches()){
            tv_user.error = "Please enter a valid email"
            tv_user.requestFocus()
            return
        }
        if(tv_password.text.toString().isEmpty()){
            tv_password.error = "Please enter password"
            tv_password.requestFocus()
            return
        }

        auth.signInWithEmailAndPassword(tv_user.text.toString(), tv_password.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    val user = auth.currentUser

                    val bundle = Bundle()
                    bundle.putString(FirebaseAnalytics.Param.METHOD, "Email")
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle)
                   goToMain()
                 /*   updateUI(user)*/
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                 /*   updateUI(null)*/
                }

                // ...
            }
    }

    /* private fun initSignInButton() {
         val googleSignInButton: Button = findViewById(R.id.login)
         googleSignInButton.setOnClickListener { v: View? -> signIn() }
     }

     private fun initAuthViewModel() {
         val authViewModel = ViewModelProvider(this).get<AuthViewModel>(AuthViewModel::class.java)
     }

     private fun initGoogleSignInClient() {
         val googleSignInOptions =
             GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                 .requestIdToken(getString(R.string.default_web_client_id))
                 .requestEmail()
                 .build()
         val googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
     }

     private fun signIn() {
         val signInIntent: Intent = googleSignInClient.getSignInIntent()
         startActivityForResult(signInIntent, RC_SIGN_IN)
     }

     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
         super.onActivityResult(requestCode, resultCode, data)
         if (requestCode == RC_SIGN_IN) {
             val task: Task<GoogleSignInAccount> =
                 GoogleSignIn.getSignedInAccountFromIntent(data)
             try {
                 val googleSignInAccount = task.getResult(
                     ApiException::class.java
                 )
                 googleSignInAccount?.let { getGoogleAuthCredential(it) }
             } catch (e: ApiException) {
                 logErrorMessage(e.message)
             }
         }
     }
     private fun getGoogleAuthCredential(googleSignInAccount: GoogleSignInAccount) {
         val googleTokenId = googleSignInAccount.idToken
         val googleAuthCredential =
             GoogleAuthProvider.getCredential(googleTokenId, null)
         signInWithGoogleAuthCredential(googleAuthCredential)
     }

     private fun signInWithGoogleAuthCredential(googleAuthCredential: AuthCredential){
         authViewModel.signInWithGoogle(googleAuthCredential)
         authViewModel.authenticatedUserLiveData.observe(this, Observer {authenticatedUser -> {
                 createNewUser(authenticatedUser)
             }
             else{
                 goToMainActivity(authenticatedUser)
             }
         }})

     private fun createNewUser(authenticatedUser: User){
         authViewModel.createUser(authenticatedUser)
         authViewModel.createdUserLiveData.observe(this, Observer { user -> {
             if(user.isCreated){
                 toastMessage(user.name)
             }
             goToMainActivity(user)
         } })
     }
     }*/

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
      /*  if (currentUser!=null) updateUI(currentUser)*/
    }

    fun goToMain(){
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        startActivity(intent)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("dasjdo",requestCode.toString()+resultCode.toString())
        // Check which request we're responding to
        if (requestCode==1) {
            // Make sure the request was successful
            if (resultCode == 1) {
                // The user picked a contact.

                // The Intent's data Uri identifies which contact was selected.
                goToMain()
                // Do something with the contact here (bigger example below)
            }
        }
    }

}
