package com.example.turista.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import com.example.turista.R
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()
        btn_sign_up2.setOnClickListener{
            signUpUser()
        }

    }

    private fun signUpUser(){
        if(tv_user.text.toString().isEmpty()){
            tv_user.error = "Please enter email"
            tv_user.requestFocus()
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(tv_user.text.toString()).matches()){
            tv_user.error = "Please enter a valid email"
            tv_user.requestFocus()
            return
        }
        if(tv_password.text.toString().isEmpty()){
            tv_password.error = "Please enter password"
            tv_password.requestFocus()
            return
        }

        auth.createUserWithEmailAndPassword(tv_user.text.toString(), tv_password.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Evento", "createUserWithEmail:success")
                    val user = auth.currentUser
                    val intent = Intent()
                    setResult(1, intent)
                    val bundle = Bundle()
                    bundle.putString(FirebaseAnalytics.Param.METHOD, "email")
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle)
                    super.onBackPressed()

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Evento", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }

                // ...
            }
    }

}
