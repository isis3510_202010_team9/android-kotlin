package com.example.turista.activities

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

fun  getBitmapFromURL(src:String) : Bitmap? {
    return try {
        var url : URL = URL(src);
        var connection : HttpURLConnection = url.openConnection() as HttpURLConnection;
        connection.doInput = true;
        connection.connect();
        var input : InputStream = connection.inputStream;
        var myBitmap : Bitmap = BitmapFactory.decodeStream(input);
        myBitmap;
    } catch (e : IOException) {
        // Log exception
        null;
    }
}