package com.example.turista.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.turista.R
import com.example.turista.adapters.LanguageAdapter
import com.example.turista.model.Language
import com.example.turista.viewmodel.LanguageViewModel
import com.example.turista.viewmodel.TuristaViewModel
import com.google.firebase.analytics.FirebaseAnalytics

class FromToLanguage : AppCompatActivity() {

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var areModelsFetched: Boolean = true
    private lateinit var languageViewModel: LanguageViewModel
    private lateinit var recyclerView : RecyclerView
    private lateinit var recyclerViewAdapter : LanguageAdapter
    private lateinit var recyclerViewLayoutManager : RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_from_language)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        languageViewModel = ViewModelProvider(this).get(LanguageViewModel::class.java)
        recyclerView = findViewById(R.id.from_to_recycler_view)
        recyclerView.setHasFixedSize(true)
        recyclerViewLayoutManager = LinearLayoutManager(this)
        recyclerViewAdapter = LanguageAdapter(this)


        recyclerView.layoutManager = recyclerViewLayoutManager
        recyclerView.adapter = recyclerViewAdapter

        supportActionBar?.setTitle(intent.extras?.getString("title"))

        Log.d("1 ", intent.extras?.getStringArrayList("Fetched").toString())

        var fetchedModels = intent.extras?.getStringArrayList("Fetched")
        if (fetchedModels != null) {
            for(language in fetchedModels){
                updateLanguage(language,"DOWNLOADED")
                Log.d("2 ", language)
/*
                Log.d("3 ", languageViewModel.allLanguages.value?.get(11)?.status)
*/
                areModelsFetched = true
            }
            recyclerViewAdapter.notifyDataSetChanged()
        }

        languageViewModel.allLanguages.observe(this, Observer { languages ->
            // Update the cached copy of the words in the adapter.
            languages?.let { recyclerViewAdapter.setLanguages(it) }
        })

        recyclerViewAdapter.onItemClick = { pos, view ->

            if(areModelsFetched){



                var lan : Language? = languageViewModel.allLanguages.value?.get(pos)
                Log.d("4 ", lan.toString() + " " + pos)
                val intent = Intent()

                var netstatus : Boolean = false
                Thread {
                    //Do some Network Request
                    var ie = InternetCheck()
                    netstatus = ie.net()
                    runOnUiThread {
                        if (lan != null) {
                            Log.d("5 Fromto", lan.toString())

                            intent.putExtra("language_choosen_code", lan.code)
                            intent.putExtra("language_choosen_language", lan.language)
                            intent.putExtra("language_status", lan.status)
                            intent.putExtra("internet", netstatus)
                            Log.d("6 Fromto", lan.toString())

                        }

                        setResult(Activity.RESULT_OK, intent)
                        Log.d("7 Fromto", lan.toString())
                        super.onBackPressed()
                    }
                }.start()


                //InternetCheck{ netstatus-> Log.d("Connection", "Is connection enabled? "+netstatus)}



            }



        }



    }

    private fun updateLanguage(code :String, newStatus:String){
        languageViewModel.update(code,newStatus)
    }




}
