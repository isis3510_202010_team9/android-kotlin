package com.example.turista.activities

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.turista.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_auth2.*
import kotlinx.android.synthetic.main.activity_tip.*

class TipActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var policy = StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_tip)

        var tp = intent.getStringExtra("transport")
        if (tp != null) {
            setCorrectTips(tp)
        }
    }
    private fun setCorrectTips(transportation: String){

        if(transportation == "sitp") {
            imagetips.setImageDrawable(getResources().getDrawable(R.drawable.sitp))
            title2.text = "Sitp"
            title3.text = "Public Bus System"
            tips1.text = "1. Locate your bus stop previously"
            tips2.text = "2. Remember to charge money on the bus card"
            tips3.text = "3. Keep an eye on windows during slow traffic"
            tips4.text = "4. Stand up time ahead of your last stop"
        }
        if(transportation == "tm") {
            imagetips.setImageDrawable(getResources().getDrawable(R.drawable.transmi))
            title2.text = "Transmilenio"
            title3.text = "BRT System"
            tips1.text = "1. Check the route prior to arriving the station"
            tips2.text = "2. Remember to charge money on the bus card"
            tips3.text = "3. Keep your personal items at your sight"
            tips4.text = "4. You can ask for help a cop if you feel lost"
        }
        if(transportation == "tax") {
            imagetips.setImageDrawable(getResources().getDrawable(R.drawable.taxi))
            title2.text = "Taxi"
            title3.text = "City Taxi"
            tips1.text = "1. Use authorized apps or phone numbers for booking"
            tips2.text = "2. Take your taxis in authorized places"
            tips3.text = "3. Check the price in the price's chart "
            tips4.text = "4. Double check you don't leave any item behind"
        }
        if(transportation == "sitpprov") {
            imagetips.setImageDrawable(getResources().getDrawable(R.drawable.sitp_prov))
            title2.text = "Provisional Sitp"
            title3.text = "Old Bus System"
            tips1.text = "1. Buses stop wherever but they follow a route"
            tips2.text = "2. Keep change and small bills"
            tips3.text = "3. Hold tight, they may drive rudely "
            tips4.text = "4. Stand up time ahead of your last stop"
        }
        if(transportation == "walk") {
            imagetips.setImageDrawable(getResources().getDrawable(R.drawable.walk))
            title2.text = "Walking"
            title3.text = "Two Reliable legs"
            tips1.text = "1. Ask previously if the place is safe to walk"
            tips2.text = "2. Don't show jewelry or value items at plain sight"
            tips3.text = "3. Drivers won't stop before you, you have to wait "
            tips4.text = "4. Cross only on street corners and zebra crossings"
        }

        if(transportation == "general") {
            imagetips.setImageDrawable(getResources().getDrawable(R.drawable.walk))
            title2.text = "Bogotá"
            title3.text = "South American Athens"
            tips1.text = "1. Plan ahead your trip, but don't discard new ideas"
            tips2.text = "2. Keep change and bills, some places do not receive credit card"
            tips3.text = "3. Be alert at all time, double check before you cross a street "
            tips4.text = "4. Have a spare jacket or umbrella, weather is always a surprise"
        }

    }
}