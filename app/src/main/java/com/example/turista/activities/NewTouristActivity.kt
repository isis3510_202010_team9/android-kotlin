package com.example.turista.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import com.example.turista.R
import com.example.turista.model.Turista

class NewTouristActivity : AppCompatActivity() {

    private lateinit var editTouristView: EditText
    private lateinit var editTouristView2: EditText
    private lateinit var editTouristView3: EditText

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_tourist)
        editTouristView = findViewById(R.id.edit_tourist)
        editTouristView2 = findViewById(R.id.edit_tourist2)
        editTouristView3 = findViewById(R.id.edit_tourist3)

        val button = findViewById<Button>(R.id.button_save)
        button.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(editTouristView.text)) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                val tourist = arrayOf(editTouristView.text.toString(),
                    editTouristView2.text.toString(),
                editTouristView3.text.toString())

                replyIntent.putExtra(EXTRA_REPLY, tourist)

                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }

    companion object {
        const val EXTRA_REPLY = "com.example.android.turistalistsql.REPLY"
    }
}