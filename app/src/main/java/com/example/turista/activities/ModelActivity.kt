package com.example.turista.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.turista.R
import com.example.turista.adapters.TuristaAdapter
import com.example.turista.model.Turista
import com.example.turista.viewmodel.TuristaViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ModelActivity : AppCompatActivity() {

    private lateinit var turistaViewModel: TuristaViewModel
    private val newWordActivityRequestCode = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_model)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = TuristaAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        turistaViewModel = ViewModelProvider(this).get(TuristaViewModel::class.java)

        turistaViewModel.allTourists.observe(this, Observer { turistas ->
            // Update the cached copy of the words in the adapter.
            turistas?.let { adapter.setTuristas(it) }
        })

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val intent = Intent(this@ModelActivity, NewTouristActivity::class.java)
            startActivityForResult(intent, newWordActivityRequestCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newWordActivityRequestCode && resultCode == Activity.RESULT_OK) {
            data?.getStringArrayExtra(NewTouristActivity.EXTRA_REPLY)?.let {
                val turista = Turista(it[0], it[1], it[2])
                turistaViewModel.insert(turista)
            }
        } else {
            Toast.makeText(
                applicationContext,
                R.string.empty_not_saved,
                Toast.LENGTH_LONG).show()
        }
    }
}
