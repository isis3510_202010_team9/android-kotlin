package com.example.turista.activities

import android.R.attr.key
import android.app.PendingIntent
import android.content.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.net.NetworkRequest
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.net.wifi.WifiManager.EXTRA_NETWORK_INFO
import android.net.wifi.WifiManager.STATUS_NETWORK_SUGGESTIONS_SUCCESS
import android.net.wifi.WifiNetworkSpecifier
import android.net.wifi.WifiNetworkSuggestion
import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.turista.R
import com.example.turista.databinding.ActivityMainBinding
import com.example.turista.fragments.HomeFragment
import com.example.turista.fragments.SearchFragment
import com.example.turista.fragments.TranslateFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.jar.Manifest
import kotlin.experimental.and
import kotlin.text.Charsets.UTF_8


var PERMISSION_ALL : Int   = 1;
val showAd: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
private lateinit var firebaseAnalytics: FirebaseAnalytics

private lateinit var credentials : ArrayList<String>
private const val TAG = "MyBroadcastReceiver"
private var ssidNfc = ""
private var messageShowed = false
class MyBroadcastReceiver : BroadcastReceiver() {


    override fun onReceive(context: Context, intent: Intent) {

        var manager : WifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager;
        Log.d("INTENT", intent.action.toString())
        Log.d("INTENTNFC", "\""+ssidNfc+"\"")


        StringBuilder().apply {
            append("Action: ${intent.action}\n")
            append("URI: ${intent.toUri(Intent.URI_INTENT_SCHEME)}\n")
            toString().also { log ->
                Log.d(TAG, log)
                /*Toast.makeText(context, log, Toast.LENGTH_LONG).show()*/
                Log.d("NFCSSID", "\""+ssidNfc+"\"")
                Log.d("SSID", manager.connectionInfo.ssid)
                Log.d("MessageShowed", messageShowed.toString())
                if((manager.connectionInfo.ssid == "\""+ssidNfc+"\"")&&(!messageShowed))
                {
                        showAd.postValue(true)


                        /*Toast.makeText(context, "Te has conectado gracias al Centro Comercial El Retiro",
                            Toast.LENGTH_LONG).show()*/
                        /*var builder : AlertDialog.Builder = AlertDialog.Builder(context)
                        builder.setCancelable(true)
                        var myBitmap : Bitmap = getBitmapFromURL(credentials[2])!!//get your bitmap
                        Thread.sleep(2000)
                        builder.setIcon(R.drawable.ad);
                        builder.setTitle("AD");

                        builder.setPositiveButton("Accept") { dialogInterface: DialogInterface, i: Int ->

                            fun onClick(dialog : DialogInterface , which : Int ){
                                dialog.dismiss();
                            }
                        };

                        var alert : AlertDialog  =builder.create();
                        alert.show();*/
                        messageShowed = true

                }
            }
        }
    }
}

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    // NFC adapter for checking NFC state in the device
    private var nfcAdapter : NfcAdapter? = null




    // Pending intent for NFC intent foreground dispatch.
    // Used to read all NDEF tags while the app is running in the foreground.
    private var nfcPendingIntent: PendingIntent? = null
    // Optional: filter NDEF tags this app receives through the pending intent.
    //private var nfcIntentFilters: Array<IntentFilter>? = null


    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate(savedInstanceState: Bundle?) {
        //hacer pantalla carga
        super.onCreate(savedInstanceState)
        var auth: FirebaseAuth = FirebaseAuth.getInstance()
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        showAd.postValue(false)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        showAd.observe(this, Observer { show ->
            // Update the cached copy of the words in the adapter.
            if(show)
            {   val bundle = Bundle()
                var builder : AlertDialog.Builder = AlertDialog.Builder(this)
                        builder.setCancelable(true)
                        /*var myBitmap : Bitmap = getBitmapFromURL(credentials[2])!!//get your bitmap
                        Thread.sleep(2000)*/
                        var startDate = 0L
                        var image : ImageView =  ImageView(this);
                        image.setImageResource(R.drawable.ad);
                        builder.setTitle("You have been connected through Touristapp!");
                        builder.setView(image)

                        builder.setPositiveButton("Accept") { dialog: DialogInterface, i: Int ->


                                Log.d("SI LLEGO", "AAAAAAAAAAAAAA")
                                bundle.putString(FirebaseAnalytics.Param.END_DATE,
                                    System.currentTimeMillis().toString())
                                val timeWathed = (System.currentTimeMillis().toDouble() - startDate.toDouble())/1000.0
                                bundle.putDouble("time_watched", timeWathed)
                                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle)
                                dialog.dismiss();

                        };

                        var alert : AlertDialog  =builder.create();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Ad CC Andino")
                        startDate = System.currentTimeMillis()
                        bundle.putString(FirebaseAnalytics.Param.START_DATE,
                            System.currentTimeMillis().toString())
                        alert.show();



            }
        })
        if(auth==null||auth.currentUser==null){
            val intent = Intent(this, AuthActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)

            Log.d("DJASD", "Voy a mandar un Intent")
            startActivity(intent)
        }
        else{

            setContentView(view)
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_WIFI_STATE
                , android.Manifest.permission.NFC, android.Manifest.permission.CHANGE_WIFI_STATE, android.Manifest.permission.ACCESS_NETWORK_STATE
                , android.Manifest.permission.INTERNET, android.Manifest.permission.SYSTEM_ALERT_WINDOW),
                PERMISSION_ALL)

            /*setSupportActionBar(toolbar)

            // Now get the support action bar
            val actionBar = supportActionBar

            // Set toolbar title/app title
            actionBar!!.title = "Hello APP"

            // Set action bar/toolbar sub title
            actionBar.subtitle = "App subtitle"

            // Set action bar elevation
            actionBar.elevation = 4.0F

            // Display the app icon in action bar/toolbar
            actionBar.setDisplayShowHomeEnabled(true)
            actionBar.setLogo(R.mipmap.ic_launcher)
            actionBar.setDisplayUseLogoEnabled(true)

            //Comentar hasta aquí*/

            var nfcAdapter = NfcAdapter.getDefaultAdapter(this)
            Log.d("NFC supported", (nfcAdapter != null).toString())
            Log.d("NFC enabled", (nfcAdapter?.isEnabled).toString())

            if(nfcAdapter == null) {
                Toast.makeText(baseContext, "Este dispositivo no cuenta con la tecnología NFC",
                    Toast.LENGTH_LONG).show()
            }

            // Read all tags when app is running and in the foreground
            // Create a generic PendingIntent that will be deliver to this activity. The NFC stack
            // will fill in the intent with the details of the discovered tag before delivering to
            // this activity.
            nfcPendingIntent = PendingIntent.getActivity(this, 0,
                Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0)

            credentials = ArrayList<String>()
            modelo_button.setOnClickListener { view ->
                val intent = Intent(this, ModelActivity::class.java)
                startActivity(intent)
            }
            initViews()

            if (intent != null) {
                // Check if the app was started via an NDEF intent
                Log.d("Found intent in onCreate", intent.action.toString())
                processIntent(intent)
            }
        }

        }
    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun changeFragment(fragment: Fragment,tagFragmentName: String?) {
        val mFragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = mFragmentManager.beginTransaction()
        val currentFragment: Fragment? = mFragmentManager.primaryNavigationFragment
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }
        var fragmentTemp =
            mFragmentManager.findFragmentByTag(tagFragmentName)
        if (fragmentTemp == null) {
            fragmentTemp = fragment
            fragmentTransaction.add(R.id.main_container, fragmentTemp, tagFragmentName)
        } else {
            fragmentTransaction.show(fragmentTemp)
        }
        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp)
        fragmentTransaction.setReorderingAllowed(true)
        fragmentTransaction.commitNowAllowingStateLoss()
    }

    private fun initViews() {
        binding.barraMenu.setOnNavigationItemSelectedListener { item ->
            val selectedFragment: Fragment? = null
            when (item.getItemId()) {
                R.id.action_home -> {
                    changeFragment(
                        HomeFragment(), HomeFragment::class.java
                            .getSimpleName()
                    )
                    /*toggleViews(true, "")*/
                }
                R.id.action_search -> {
                    changeFragment(
                        SearchFragment(), SearchFragment::class.java
                            .getSimpleName()
                    )
                    /* toggleViews(false, "Favorites")*/
                }
                R.id.action_translate -> {
                    changeFragment(TranslateFragment(), TranslateFragment::class.java.getSimpleName())
                    /*toggleViews(false, "Venues")*/
                }

            }
            true}
            changeFragment(
                    HomeFragment(), HomeFragment::class.java
            .getSimpleName()
            )
        }



    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Log.d("Found intent in onNewIntent", intent?.action.toString())
        if (intent != null) {
            processIntent(intent)
        }

    }


    /**
     * Check if the Intent has the action "ACTION_NDEF_DISCOVERED". If yes, handle it
     * accordingly and parse the NDEF messages.
     * @param checkIntent the intent to parse and handle if it's the right type
     */

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun processIntent(checkIntent: Intent) {
        // Check if intent has the action of a discovered NFC tag
        // with NDEF formatted contents
        if (checkIntent.action == NfcAdapter.ACTION_NDEF_DISCOVERED) {
            Log.d("New NDEF intent", checkIntent.toString())

            // Retrieve the raw NDEF message from the tag
            val rawMessages = checkIntent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
            Log.d("Raw messages", rawMessages.size.toString())

            // Complete variant: parse NDEF messages
            if (rawMessages != null) {
                val messages = arrayOfNulls<NdefMessage?>(rawMessages.size)// Array<NdefMessage>(rawMessages.size, {})
                for (i in rawMessages.indices) {
                    messages[i] = rawMessages[i] as NdefMessage;
                }
                // Process the messages array.
                processNdefMessages(messages)
            }

/*            //Conectarse a la red
            var builderNS = WifiNetworkSpecifier.Builder()
            builderNS.setSsid("abcdefgh");
            builderNS.setWpa2Passphrase("1234567890");

            var wifiNetworkSpecifier : WifiNetworkSpecifier = builderNS.build();
            var networkRequestBuilder = NetworkRequest.Builder()
            networkRequestBuilder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
            networkRequestBuilder.addCapability(NetworkCapabilities.NET_CAPABILITY_NOT_RESTRICTED);
            networkRequestBuilder.addCapability(NetworkCapabilities.NET_CAPABILITY_TRUSTED);
            networkRequestBuilder.setNetworkSpecifier(wifiNetworkSpecifier);
            var networkRequest = networkRequestBuilder.build();
            var cm = getSystemService(Context.CONNECTIVITY_SERVICE)
            if (cm != null) {
                cm.requestNetwork(networkRequest, new ConnectivityManager.NetworkCallback() {
                    @Override
                    public void onAvailable(@NonNull Network network) {
                        //Use this network object to Send request.
                        //eg - Using OkHttp library to create a service request

                        super.onAvailable(network);
                    }
                });*/
            //Conectarse a la red wifi
            Log.d("RED", "INTENTO CONECTARME A LA RED")
            var builder = WifiNetworkSuggestion.Builder()
                .setSsid(credentials[0])
                .setWpa2Passphrase(credentials[1])

            var suggestion : WifiNetworkSuggestion = builder.build();

            var list =  ArrayList<WifiNetworkSuggestion>()
            list.add(suggestion);

            var manager : WifiManager = getSystemService(Context.WIFI_SERVICE) as WifiManager;
            manager.connectionInfo.bssid
            var status = manager.addNetworkSuggestions(list);
            ssidNfc = "ORTEGAR"
            if (status == STATUS_NETWORK_SUGGESTIONS_SUCCESS) {
                //We have successfully added our wifi for the system to consider
                Log.d("RED", "Se logró añadir")
            }

            // Optional (Wait for post connection broadcast to one of your suggestions)
            val intentFilter = IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);

            val broadcastReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    if (!intent.action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                        return;
                    }
                    // do post connect processing here
                    if((manager.connectionInfo.ssid == "\""+ssidNfc+"\"")&&(!messageShowed))
                    {
                        showAd.postValue(true)


                        Toast.makeText(context, "Te has conectado gracias al Centro Comercial El Retiro",
                            Toast.LENGTH_LONG).show()
                        /*var builder : AlertDialog.Builder = AlertDialog.Builder(context)
                        builder.setCancelable(true)
                        var myBitmap : Bitmap = getBitmapFromURL(credentials[2])!!//get your bitmap
                        Thread.sleep(2000)
                        builder.setIcon(R.drawable.ad);
                        builder.setTitle("AD");

                        builder.setPositiveButton("Accept") { dialogInterface: DialogInterface, i: Int ->

                            fun onClick(dialog : DialogInterface , which : Int ){
                                dialog.dismiss();
                            }
                        };

                        var alert : AlertDialog  =builder.create();
                        alert.show();*/
                        messageShowed = true

                    }
                }
            };
            registerReceiver(broadcastReceiver, intentFilter);


            // Simple variant: assume we have 1x URI record
            //if (rawMessages != null && rawMessages.isNotEmpty()) {
            //    val ndefMsg = rawMessages[0] as NdefMessage
            //    if (ndefMsg.records != null && ndefMsg.records.isNotEmpty()) {
            //        val ndefRecord = ndefMsg.records[0]
            //        if (ndefRecord.toUri() != null) {
            //            logMessage("URI detected", ndefRecord.toUri().toString())
            //        } else {
            //            // Other NFC Tags
            //            logMessage("Payload", ndefRecord.payload.contentToString())
            //        }
            //    }
            //}

        }
    }

    /**
     * Parse the NDEF message contents and print these to the on-screen log.
     */
    private fun processNdefMessages(ndefMessages: Array<NdefMessage?>) {
        // Go through all NDEF messages found on the NFC tag

        for (curMsg in ndefMessages) {
            if (curMsg != null) {
                // Print generic information about the NDEF message
                Log.d("Message", curMsg.toString())
                // The NDEF message usually contains 1+ records - print the number of recoreds
                Log.d("Records", curMsg.records.size.toString())

                // Loop through all the records contained in the message
                for (curRecord in curMsg.records) {
                    if (curRecord.toUri() != null) {
                        // URI NDEF Tag
                        Log.d("- URI", curRecord.toString())
                    } else {
                        // Other NDEF Tags - simply print the payload
                        Log.d("- Contents", curRecord.payload.toString())
                        val payload = curRecord.payload
                        val languageCodeLength = (payload[0] and 51).toInt()
                        val text = String(
                            payload,
                            languageCodeLength + 1,
                            payload.size - languageCodeLength - 1,
                            UTF_8
                        )
                        Log.d("MESSAGE", text)
                        credentials.add(text)
                        Log.d("Credentials", credentials.toString())


                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("ON RESUME","RESUMEEEEEEEEEEEEEE")
        nfcAdapter?.enableForegroundDispatch(this, nfcPendingIntent, null, null);

        /*NfcAdapter.getDefaultAdapter(this)?.let { nfcAdapter ->
            // An Intent to start your current Activity. Flag to singleTop
            // to imply that it should only be delivered to the current
            // instance rather than starting a new instance of the Activity.
            val launchIntent = Intent(this, this.javaClass)
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)

            // Supply this launch intent as the PendingIntent, set to cancel
            // one if it's already in progress. It never should be.
            val pendingIntent = PendingIntent.getActivity(
                this, 0, launchIntent, PendingIntent.FLAG_CANCEL_CURRENT
            )

            // Define your filters and desired technology types
            val filters = arrayOf(IntentFilter(ACTION_NDEF_DISCOVERED))
            val techTypes = arrayOf(arrayOf(IsoDep::class.java.name))

            // And enable your Activity to receive NFC events. Note that there
            // is no need to manually disable dispatch in onPause() as the system
            // very strictly performs this for you. You only need to disable
            // dispatch if you don't want to receive tags while resumed.
            nfcAdapter.enableForegroundDispatch(
                this, pendingIntent, filters, techTypes
            )
        }*/
    }
    override fun onPause() {
        super.onPause()
        // Disable foreground dispatch, as this activity is no longer in the foreground
        nfcAdapter?.disableForegroundDispatch(this);
    }

    public fun  getBitmapFromURL(src:String) : Bitmap? {
        return try {
            var url : URL = URL(src);
            var connection : HttpURLConnection = url.openConnection() as HttpURLConnection;
            connection.doInput = true;
            connection.connect();
            var input : InputStream = connection.inputStream;
            var myBitmap : Bitmap = BitmapFactory.decodeStream(input);
            myBitmap;
        } catch (e : IOException) {
            // Log exception
            null;
        }
}





    }




