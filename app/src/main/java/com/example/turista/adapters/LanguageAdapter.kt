package com.example.turista.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.turista.R
import com.example.turista.model.Language

class LanguageAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<LanguageAdapter.LanguageViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var languages = emptyList<Language>() // Cached copy of words
    var onItemClick: ((pos: Int, view: View) -> Unit)? = null

    inner class LanguageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val languageTextView: TextView = itemView.findViewById(R.id.tv_translate1)
        val languageTextView2: TextView = itemView.findViewById(R.id.tv_translate2)
        val languageStatusImageView: ImageView = itemView.findViewById(R.id.imv_translate)
        override fun onClick(v: View?) {
            if (v != null) {
                onItemClick?.invoke(adapterPosition, v)
            }
        }
        init {
            itemView.setOnClickListener(this)
        }
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageViewHolder {
        val itemView = inflater.inflate(R.layout.single_item_translate, parent, false)
        return LanguageViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: LanguageViewHolder, position: Int) {
        val current = languages[position]
        holder.languageTextView.text = current.language
        holder.languageTextView2.text = "("+current.code+")"
        if(current.status.equals("NO_DOWNLOADED")) {
            holder.languageStatusImageView.setImageResource(R.drawable.ic_cloud_download)
        }
        else
            holder.languageStatusImageView.setImageResource(R.drawable.ic_check)

    }

    internal fun setLanguages(language: List<Language>) {
        this.languages = language
        notifyDataSetChanged()

    }

    override fun getItemCount() = languages.size
}