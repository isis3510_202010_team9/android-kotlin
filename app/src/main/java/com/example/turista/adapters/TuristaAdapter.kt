package com.example.turista.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.turista.R
import com.example.turista.model.Turista

class TuristaAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<TuristaAdapter.TuristaViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var turistas = emptyList<Turista>() // Cached copy of words

    inner class TuristaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val turistaItemView: TextView = itemView.findViewById(R.id.textView)
        val turistaItemView2: TextView = itemView.findViewById(R.id.textView2)
        val turistaItemView3: TextView = itemView.findViewById(R.id.textView3)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TuristaViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return TuristaViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TuristaViewHolder, position: Int) {
        val current = turistas[position]
        holder.turistaItemView.text = current.id
        holder.turistaItemView2.text = current.firstName
        holder.turistaItemView3.text = current.lastName
    }

    internal fun setTuristas(turistas: List<Turista>) {
        this.turistas = turistas
        notifyDataSetChanged()
    }

    override fun getItemCount() = turistas.size
}